FROM python:3.9-slim-buster

# Creating a non root user
RUN useradd -u 1001 ecosia-user
RUN mkdir /ecosia
WORKDIR /ecosia

COPY ./requirements.txt /ecosia
RUN pip install --no-cache-dir --upgrade -r /ecosia/requirements.txt
COPY ./app /ecosia/app

USER ecosia-user
EXPOSE 80
ENTRYPOINT [ "uvicorn" ]
CMD [ "app.main:app", "--host", "0.0.0.0", "--port", "80"]