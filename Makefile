# Install dependencies
# Regenerate
dependencies:
	@poetry export > requirements.txt || true
	@pip install -r requirements.txt

# Launch tests
test: dependencies
	@pytest

# Run the app locally
run-local: dependencies
	@uvicorn app.main:app --reload

# Build Docker image
build-docker:
	@docker build -t ecosia/webserver:latest .

# Run Docker container locally
# Stopping and deleting potential existing container
run-docker: build-docker
	@docker stop ecosia-webserver || true
	@docker rm ecosia-webserver || true
	@docker run --name ecosia-webserver -d -p 8000:80 ecosia/webserver:latest

# Deploy the app on minikube
# Waiting for it to be healthy before using curl
deploy: build-docker
	@kubectl apply -f kubernetes.yaml
	until $$(curl --output /dev/null --silent --head --fail http://local.ecosia.org/tree) ; do \
			printf '.' ; \
			sleep 5 ; \
	done
	echo "Webserver available at local.ecosia.org"
