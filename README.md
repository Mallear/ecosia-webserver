# ecosia-webserver

![Pipeline status](https://gitlab.com/Mallear/ecosia-webserver/badges/main/pipeline.svg)

## Application structure

This a simple Python webserver made with FastAPI.

It accepts GET requests on the path `/tree` and `/status`. The first one return the following JSON response:

```json
{ "myFavouriteTree": "Eucalyptus" }
```

The `/status` path is used as a simple health check.

## Local development

This project uses a `Makefile` to make local development easier.

### Dependencies

This project is made using Python3.9 and FastAPI.

Dependencies management is made using Poetry, a `requirements.txt` is exported by Poetry to be able to run this project with other package managers.

Init the project locally:

```bash
# Using Makefile
$ make dependencies
```

To run the application locally:

```bash
# Using Makefile
$ make run-local
```

### Running tests

Tests are developed using FastAPI embedded Pytest classes.

```bash
# Using Makefile
$ make test
```

### Dockerizing the application

Given Dockerfile build an image for running the webserver as a non root user.

To build and run the image:

```bash
# Build image using Makefile
$ make build-docker

# Build and run image using Makefile
$ make run-docker
```

## Tilt.dev

To make development easier on Kubernetes, this project provides a `Tiltfile` which provides live updates to kubernetes resources when the application source code, the kubernetes resources definition, the Dockerfile or the dependencies change.

To use it, follow [Tilt.dev install process](https://docs.tilt.dev/install.html) and juste run `tilt up`. It will build the image and launch a pod to your cluster which will be updated whenever the source code is updated.

Note: to enable code live updates, add `"--reload"` in the Dockerfile `CMD` instructions.

## Deployment

**Note:** the deployment process assumes you are deploying on minikube with the ingress add-on enabled, using minikube Docker environment (`minikube docker-env | source`) and you setup your `/etc/hosts` to redirect `local.ecosia.org` to your minikube cluster IP (`minikube ip`).

Kubernetes resources manifest needed to deploy this app can be found in the `kubernetes.yaml` file.

```bash
# Deploying the app using Makefile
$ make deploy
```

The deployment process will build the Docker image and wait until the app is available at [https://local.ecosia.org/tree](https://local.ecosia.org/tree).
