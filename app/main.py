from fastapi import FastAPI

app = FastAPI()

@app.head("/tree")
@app.get("/tree")
async def tree():
    return {"myFavouriteTree": "Eucalyptus"}


@app.get("/status")
async def status():
    return {"Status": "Up"}
