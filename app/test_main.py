from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)


def test_read_tree():
    response = client.get("/tree")
    assert response.status_code == 200
    assert response.json() == {"myFavouriteTree": "Eucalyptus"}

def test_read_status():
    response = client.get("/status")
    assert response.status_code == 200
    assert response.json() == {"Status": "Up"}